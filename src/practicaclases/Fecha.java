/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaclases;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 39322011
 */
public class Fecha {
    private int anio;
    private int mes;
    private int dia;
    final private int[] maximDias = {31,28,31,30,31,30,31,31,30,31,30,31};

    public Fecha(){
        this.anio = 0;
        this.mes = 1;
        this.dia = 1;
    }
    
    /**
     * 
     * @param dia El dia de setteo
     * @param mes El mes de setteo
     * @param anio El año de setteo
     */
    public Fecha(int dia,int mes,int anio) {
        this.anio = 0;
        this.mes = 1;
        this.dia = 1;
        setValues(dia, mes, anio);
    }
    
    /**
     * 
     * @return El año
     */
    public int getAnio() {
        return this.anio;
    }
    
    /**
     * 
     * @return El mes
     */
    public int getMes() {
        return this.mes;
    }
    
    public int getDia() {
        return this.dia;
    }
    
    /**
     * 
     * @param anio 
     */
    public void setAnio(int anio) {
        testValues(this.dia, this.mes, anio);
        this.anio = anio;
    }
    
    public void setMes(int mes) {
        testValues(this.dia, mes, this.anio);
        this.mes = mes;
    }
    
    public void setDia(int dia) {
        testValues(dia, this.mes, this.anio);
        this.dia = dia;
    }
    
    /**
     * 
     * @param dia
     * @param mes
     * @param anio 
     */
    public final void setValues(int dia, int mes, int anio) {
        testValues(dia, mes, anio);
        this.anio = anio;
        this.mes = mes;
        this.dia = dia;
    }
    
    public Fecha restarDias(int dias) {
        
        return operarFecha( fechaFromDias(dias) );
    }
    
    public Fecha operarFecha(Fecha f) {
        
        int nuevoAnio = this.anio - f.getAnio();
        int nuevoMes = this.mes - f.getMes();
        int nuevoDia = this.dia - f.getDia();
        
        if (nuevoDia < 1) {
            nuevoMes--;
            if (nuevoMes < 1) {
                nuevoMes = 12 + nuevoMes;
                nuevoAnio--;
            }
            nuevoDia = maximDias[nuevoMes-1] + nuevoDia;
        }
        
        return new Fecha(nuevoDia, nuevoMes, nuevoAnio);
    }
    
    public Fecha fechaFromDias(int dias) {
        Fecha fechaZero = new Fecha();
        return fechaZero.sumarDias(dias);
    }
    
    public Fecha sumarDias(int dias) {
        
        int nuevoAnio = this.anio + dias / 365;
        int nuevoMes = this.mes;
        int nuevoDias = this.dia + dias % 365;
        while (nuevoDias > maximDias[nuevoMes-1] ) {
            nuevoDias -= maximDias[nuevoMes-1];
            nuevoMes += 1;
            if (nuevoMes == 13){
                nuevoMes = 1;
                nuevoAnio += 1;
            }
        }
        
        return new Fecha(nuevoDias, nuevoMes, nuevoAnio);
    }
    
    public Fecha restarFecha(Fecha desde) {
        /**
         * @params desde (tiene que ser menor?)
         */
        
        Fecha d = new Fecha();
        
        if (isGreaterThan(desde))
            // this es mas grande que desde
            d = operarFecha(desde);
         else
            d = operarFecha(this);
        
        return d;
    }
    
    public Fecha sumarFecha(Fecha addFecha) {
        
        int diasDiff = 0;
        
        for ( int i = this.mes; i < addFecha.getMes() + this.mes; i++)
            diasDiff += maximDias[i % 12];
        
        diasDiff += addFecha.getDia();
        diasDiff += addFecha.getAnio() * 365;
        
        return sumarDias(diasDiff);
    }
    
    public boolean isGreaterThan() {
        Calendar c = Calendar.getInstance();
        return isGreaterThan(
                EasyCalendar.getDia(),
                EasyCalendar.getMonth(),
                EasyCalendar.getYear());
    }
    
    /**
     * @param dia El dia de la fecha
     * @param mes El mes de la fecha
     * @param anio El año de la fecha
     */
    public boolean isGreaterThan(int dia, int mes, int anio) {
        return isGreaterThan(new Fecha(dia, mes, anio));
    }
    
    /**
     * @param Fecha La fecha a comparar si es mayor
     */
    public boolean isGreaterThan(Fecha fecha) {
        
        testValues(fecha);
        
        if (this.anio > fecha.getAnio())
            return true;
        else if (this.anio == fecha.getAnio()) {
            if (this.mes > fecha.getMes())
                return true;
            else if (this.mes == fecha.getMes()) {
                if (this.dia > fecha.getDia())
                    return true;
                else if (this.dia == fecha.getDia())
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;
    }
    
    @Override
    public String toString() {
        /*
         * @return nice formated date
         */
        String format = "%s/%s/%s";
        return String.format(format,dia,mes,anio); 
    }
    
    private void testValues(Fecha fecha) {
        testValues(fecha.getDia(), fecha.getMes(), fecha.getAnio());
    }
    
    /**
     * @param dia - El dia
     * @param mes - El mes
     * @param anio - El anio
     */
    private void testValues(int dia, int mes, int  anio){
        try {
            if (anio < 0)
                throw new FechaOutOfBoundsException("Pone bien el anio");
            if (mes > 12 || mes < 1)
                throw new FechaOutOfBoundsException("Pone bien el mes");
            if (dia < 1 || dia > maximDias[mes-1])
                throw new FechaOutOfBoundsException("Pone bien el dia");
        } catch (FechaOutOfBoundsException ex) {
            Logger.getLogger(Fecha.class.getName()).log(Level.SEVERE, "YES", ex);
            System.exit(127);
        }
    }
     
}

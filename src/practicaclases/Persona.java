/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaclases;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 39322011
 */
public class Persona {
    private String nombre;
    private String apellido;
    private Fecha nacimiento;

    public Persona() {
        this.nombre = "";
        this.apellido = "";
        this.nacimiento = new Fecha();
    }

    public Persona(String nombre, String apellido, Fecha fechaNac) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nacimiento = fechaNac;
    }
    
    public String getApellido() {
        return this.apellido;
    }

    public Fecha getNacimiento() {
        return this.nacimiento;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setApellido(String apellido) {
        testValues(this.nombre, apellido, this.nacimiento);
        this.apellido = apellido;
    }

    public void setNacimiento(Fecha nacimiento) {
        testValues(this.nombre, this.apellido, nacimiento);
        this.nacimiento = nacimiento;
    }

    public void setNombre(String nombre) {
        testValues(nombre, this.apellido, this.nacimiento);
        this.nombre = nombre;
    }
    
    public int edad() {
        
        Fecha hoy = EasyCalendar.toFecha();
        
        int edad = hoy.restarFecha(this.nacimiento).getAnio();
        
        return edad;
    }
    
    @Override
    public String toString() {
        /*
         * @return nice formated date
         */
        String format = "%s %s";
        return String.format(format, this.nombre, this.apellido);
    }
    
    private void testValues(String nombre, String apellido, Fecha nacimiento){
        
        try {
            if (nombre.equals(""))
                throw new PersonaException("El nombre esta vacio");
            if (apellido.equals(""))
                throw new PersonaException("El apellido esta vacio");
            if (nacimiento.isGreaterThan())
                throw new PersonaException("Nacio despues de hoy?");
        } catch (PersonaException ex) {
            Logger.getLogger(Fecha.class.getName()).log(Level.SEVERE, "YES", ex);
            System.exit(127);
        }
        
        assert !nombre.equals("") : "Pone bien el anio Pelotudo!";
        assert !apellido.equals("") : "Pone bien el mes Pelotudo!";
        assert nacimiento.isGreaterThan() : "Pone bien el dia Pelotudo!";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaclases;

import java.util.Calendar;

/**
 *
 * @author 39322011
 */
public abstract class EasyCalendar extends Calendar {
    
    public static int getDia() {
        return getInstance().get(Calendar.DAY_OF_MONTH);
    }
    
    public static int getMonth() {
        return getInstance().get(Calendar.MONTH)+1;
    }
    
    public static int getYear() {
        return getInstance().get(Calendar.YEAR);
    }
    
    public static Fecha toFecha() {
        return new Fecha(getDia(), getMonth(), getYear());
    }
    
    
    public static String toFormattedString(String t) {
        return String.format("%s"+t+"%s"+t+"%s", getDia(), getMonth(), getYear());
    }
    
}
